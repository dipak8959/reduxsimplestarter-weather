import Axios from 'axios';

const API_KEY = 'dd20928ffa02bacd7ebd8fb0a464ef4c';
const ROOT_URL = `http://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}`;

export const FETCH_WEATHER = 'FETCH_WEATHER';


export function fetchWeather(city){

  const url = `${ROOT_URL}&q=${city},us`;
  const request = Axios.get(url);

  return
  (
    {
      type: FETCH_WEATHER, 
      payload: request
    }
  );
}
